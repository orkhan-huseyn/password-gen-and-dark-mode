generatePasssword.onclick = function () {
  const password = generatePassword(+passwordLengthInput.value);
  resultInput.value = password;
};

copyButton.onclick = function () {
  navigator.clipboard.writeText(resultInput.value);
  copyButton.textContent = 'Copied!';
};

function generatePassword(passwordLength = 0) {
  let alphabet = '';

  if (includeSymbols.checked) {
    alphabet += '+-)({}@#?';
  }

  if (includeNumbers.checked) {
    alphabet += '0123456789';
  }

  if (includeLowerCaseLetters.checked) {
    alphabet += 'abcdefghijklmnopqrstuvwxyz';
  }

  if (includeUpperCaseLetters.checked) {
    alphabet += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  }

  let password = '';
  for (let i = 0; i < passwordLength; i++) {
    const randomIndex = Math.floor(Math.random() * alphabet.length);
    password += alphabet[randomIndex];
  }

  return password;
}
